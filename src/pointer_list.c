#include <stdint.h>
#include <stdlib.h>
#include "order_tree.h"
#include "pointer_list.h"


/*
* ��������� ��������� �� ������.
* �.�. id ��������� ����������, �� ��������� ����� ��������� ������.
* ���� ��������� ������ �� �������� ��� ��� ���, �� ������� �����.
*/
void plist_insert(const Order* order) {
	uint32_t id = order->id;
	PointerList* plist = PListRoot.last;

	if (!plist || (id > plist->max_id)) {
		plist = _create_plist(id);
	}
	plist->orders[id - plist->min_id] = order;
	plist->non_null++;
}


/*
* �������� ��������� �� ������ � ���������� ���.
* ���� ������ � ����� id ��� ��� ��� ��� �������, �� ���������� NULL.
* ���� ��� ���� ��������� ����������� ������ � ������ � ���� ������ �� ���������, �� �� ���������.
*/
Order* plist_pop(const uint32_t id) {
	Order* order;
	uint32_t index;
	PointerList* plist = PListRoot.first;

	if (!plist) return NULL;

	// ���� ���������� ������
	while (id > plist->max_id) {
		plist = plist->next;
		if (!plist) return NULL;
	}
	// �������� ���������� ������ ��� ������
	// ��� ����� ������ ��������� �� ����
	if (id < plist->min_id) return NULL;

	index = id - plist->min_id;
	order = plist->orders[index];

	if (order) {
		plist->orders[index] = NULL;
		plist->non_null--;
		if (!plist->non_null && plist->next) {
			_delete_plist(plist);
		}
	}
	return order;
}


/*
* ������� ������.
* ����� ������ ������ � ��� ���� ������ �����������, � ��� ����������� id ����� id ��� ������ ������.
* ���� � ����������� ������ ��� ��������� �� ������ �������, �� �� ���������, �.�. id ��������� ����������.
*/
PointerList* _create_plist(const uint32_t id) {
	PointerList* plist = malloc(sizeof(PointerList));
	if (!plist) exit(EXIT_FAILURE);

	// ���� ���������� ���� ���� ������
	if (PListRoot.last) {
		PListRoot.last->next = plist;
		plist->prev = PListRoot.last;

		if (!PListRoot.last->non_null) {
			_delete_plist(PListRoot.last);
		}
	}
	else {
		PListRoot.first = plist;
		plist->prev = NULL;
	}
	PListRoot.last = plist;
	PListRoot.last_length *= 2;

	plist->orders = calloc(PListRoot.last_length, sizeof(PointerList));
	if (!plist->orders) exit(EXIT_FAILURE);

	plist->non_null = 0;
	plist->min_id = id;
	plist->max_id = id + PListRoot.last_length - 1;
	plist->next = NULL;
	return plist;
}


/*
* ������� ������.
* ���������������, ��� ��������� ������ �� ���������. ��� ������ ���������� ����� ������� �������.
*/
void _delete_plist(PointerList* plist) {
	if (plist->prev) {
		plist->prev->next = plist->next;
		plist->next->prev = plist->prev;
	}
	else {
		PListRoot.first = plist->next;
		plist->next->prev = NULL;
	}
	free(plist->orders);
	free(plist);
}