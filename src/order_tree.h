/*
* Order tree - �������� ������, � ������� ������ ����������� �� ����.
*
* ��������� ������� ��� ������� � ������. ��� ������ ������ ������ -
* ����� �������, ��� ������� - ����� �������.
* ��� �������� ������� � ������� �� id ���������� pointer list, ����� ��� ������� � ������.
* 
* ������ "super_parent" ��� "����� ��������" ��� "������ ������" - ��� ������ ������ � ������, 
* ����� ������� ����� ������� � "������ ������" �������� ������ � ����� "better". 
* "������ ������" ������������� �������� ��� "super_parent" "best" �������. ��� ��������������� �������.
*/


typedef struct Order {
	struct Order*		parent;				// ��������
	struct Order*		better;				// ������ ������/�������
	struct Order*		worse;				// ������ �������/������ ��� ����� ��
	struct Order*		best;				// ����� �������/������� ������ ����� ������ ������
	uint32_t			id;					// id ������
	uint32_t			qty;				// ���-�� ������
	uint32_t			price;				// ���� ������
	struct OrdersRoot*	root;				// ��������� �� ������
} Order;


typedef struct OrdersRoot {
	Order*	first;
	uint8_t side;
} OrdersRoot;


Order*	save_order		(OrdersRoot* root, const id, const uint32_t qty, const uint32_t price);
void	remove_order	(Order* order, Order* super_parent);
void	tree_insert		(Order* order);
void	_remove_order	(Order* order);
