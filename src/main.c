/*
�������� �������� �����.
------------------------------------------------------------------------
�� ����, �� ���������� ����� ��� STDIN, �������� ������ � ������ ������.
------------------------------------------------------------------------
��������� ��������� ������������ � STDOUT ��� � ��������� ����.
*/


#define _CRT_SECURE_NO_WARNINGS


#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "order_tree.h"
#include "pointer_list.h"

#ifdef DEBUG
	#include <assert.h>
#endif // DEBUG


/* -------------------------------------------------------------------------- */
/*								  DEFENITIONS	                              */
/* -------------------------------------------------------------------------- */


static uint32_t TRADE_ID = 0;

static FILE* INPUT_FD = NULL;
static FILE* OUTPUT_FD = NULL;

static OrdersRoot BUYINGS_ROOT = { .first = NULL, .side = 'B' };
static OrdersRoot SELLINGS_ROOT = { .first = NULL, .side = 'S' };


enum GeneralError {
	A_SUCCESS,
	TOO_MANY_ARGS,				// �������� ������� ����� ����������
	INVALID_ARGUMENT,			// ��������� � ����� ������ ���
	FILE_ERROR,					// ������ �����
};

enum ExchangeError {
	E_SUCCESS,
	PARSE_ERROR,				// �������� ������ ������ ��� ������
	INVALID_ORDER_PARAM,		// ���������� ��������� ������
};


/* -------------------------------------------------------------------------- */
/*									DEBUG									  */
/* -------------------------------------------------------------------------- */

// ������� ����������� ����������� ������ ������
#ifdef DEBUG

void	verify_trees		();
void	_verify_tree		(OrdersRoot* root);
void	_common_assertions	(Order* order, OrdersRoot* root);
void	_verify_worse		(Order* order, Order* parent, OrdersRoot* root);
Order*	_verify_better		(Order* order, Order* parent, OrdersRoot* root);


void _common_assertions(Order* order, OrdersRoot* root) {
	assert(order->root == root);
	assert(order->qty > 0);
	assert(order->qty < 150);
}

// ��������� ������, ������� ����� (�� ����) ������ ��������
Order* _verify_better(Order* order, Order* parent, OrdersRoot* root) {
	_common_assertions(order, root);
	assert(order->parent == parent);
	assert(order->best == NULL);

	if (root->side == 'S') assert(order->price < parent->price);
	else assert(order->price > parent->price);

	if (order->worse) _verify_worse(order->worse, order, root);
	if (order->better) {
		return _verify_better(order->better, order, root);
	}
	else return order;
}

// ��������� ������, ������� ���� (�� ����) ������ ��������
void _verify_worse(Order* order, Order* parent, OrdersRoot* root) {
	Order* best_order = order;

	_common_assertions(order, root);
	assert(order->parent == parent);

	if (root->side == 'S') assert(order->price >= parent->price);
	else assert(order->price <= parent->price);
	
	if (order->worse) _verify_worse(order->worse, order, root);
	if (order->better) {
		best_order = _verify_better(order->better, order, root);
	}
	assert(order->best == best_order);
}

void _verify_tree(OrdersRoot* root) {
	Order* order = root->first;
	Order* best_order = order;

	if (!order) {
		printf("Empty buyings tree!\n");
		return;
	}
	_common_assertions(order, root);
	assert(order->parent == NULL);

	if (order->worse) _verify_worse(order->worse, order, root);
	if (order->better) {
		best_order = _verify_better(order->better, order, root);
	}
	assert(order->best == best_order);
}

void verify_trees() {
	_verify_tree(&SELLINGS_ROOT);
	_verify_tree(&BUYINGS_ROOT);
}

#endif // DEBUG

/* -------------------------------------------------------------------------- */
/*								 ALGORITHM API								  */
/* -------------------------------------------------------------------------- */


void print_trade(
	const uint8_t side,
	const uint32_t id_1,
	const uint32_t id_2,
	const uint32_t qty,
	const uint32_t price
) {
	fprintf(OUTPUT_FD, "T,%u,%c,%u,%u,%u,%u.%u\n", 
		++TRADE_ID, side, id_1, id_2, qty, price / 100, price % 100);
}

void cancel(uint32_t id) {
	Order* order = plist_pop(id);
	if (order) {
		remove_order(order, NULL);
		fprintf(OUTPUT_FD, "X,%u\n", id);
	}
}

void trade(
	OrdersRoot* self_root,
	OrdersRoot* target_root,
	uint32_t id,
	uint32_t qty,
	const uint32_t price
) {
	// ���� ����� ������ � ������ ��� ������� �����, �� ������ �����������
	if (qty = _trade(target_root, id, qty, price)) {
		plist_insert(save_order(self_root, id, qty, price));
	}
}

/*
* �������� ������� ��������.
* � ������ �������� ������� ������ ������ � ���� � ��� ������ ������,
* ��� � ����� ������, �� ��� ��������� � ������ ������� ���������� ������.
* ������� ���������� ���������� ����������� ������ � ����� ������.
*/
uint32_t _trade(OrdersRoot* root, uint32_t id, uint32_t qty, const uint32_t price) {
	Order* best_order;
	Order* first_order;

	// ��������� ��� ����������� ������ ���� ����� ����������� ������� � �������
	// �������� ��� SELL ������ ����� ����� ��, ������� �������.
	int64_t x = (root->side == 'B') ? 1 : -1;
	int64_t y = -x;
	int64_t _price = x * (int64_t)price;
	
	while (1) {
		// ������ ������� ������ ������ ��������
		if (!(first_order = root->first)) break;
		best_order = first_order->best;

		if ((_price + (y * (int64_t)best_order->price)) > 0) break;
		
		if (qty < best_order->qty) {
			best_order->qty -= qty;
			print_trade(root->side, best_order->id, id, qty, best_order->price);
			return 0;
		}
		else {
			qty -= best_order->qty;
			print_trade(root->side, best_order->id, id, best_order->qty, best_order->price);
			remove_order(plist_pop(best_order->id), first_order);
			if (!qty) break;
		}
	}
	return qty;
}


/* -------------------------------------------------------------------------- */
/*									COMMON									  */
/* -------------------------------------------------------------------------- */


// ��������� ������ ������
void process_cancel() {
	uint32_t id;

	if (fscanf(INPUT_FD, ",%u", &id) != 1) {
		printf("Parse cancel error.");
		exit(PARSE_ERROR);
	}
	cancel(id);
}

/*
* ��������� ��������� ������, ��������� ���� ������ �� �������� ����� � �����.
* � ����������� �� side ������, ��������� ������ ������ � ��������� ��������.
*/
void process_order() {
	uint32_t id = 0;
	uint8_t side = 0;
	uint32_t qty = 0;
	uint32_t price = 0;		// ���� � ���� ������ �����
	double real_price = 0;

	// ���� �� ��� ������� ������� ���������, �� ������� � �������
	if (fscanf(INPUT_FD, ",%u,%c,%u,%lf", &id, &side, &qty, &real_price) != 4) {
		printf("Parse order error.");
		printf("%u %c %u %lf\n", id, side, qty, real_price);
		exit(PARSE_ERROR);
	}
	// �.�. � ��� �������� 2 ����� ����� �������
	price = real_price * 100;

	if (!qty || !price) {
		printf("Qty and price can not be zero. Invalid order id: %u", id);
		exit(INVALID_ORDER_PARAM);
	}

	if (side == 'B') trade(&BUYINGS_ROOT, &SELLINGS_ROOT, id, qty, price);
	//if (side == 'B') return;
	else if (side == 'S') trade(&SELLINGS_ROOT, &BUYINGS_ROOT, id, qty, price);
	//else if (side == 'S') return;
	else {
		printf("Invalid side param %c", side);
		exit(INVALID_ORDER_PARAM);
	}
}

/*
* ���������� �� ������ ������ ����� �, � ����������� �� ������� �������, ��������� ��������� ������ ��� ������.
* ��� ���������� ������ DEBUG ������ �������� ����������� ����������� �������� ������.
*/
void parse_lines() {
	int operation_type;

	while (1) {
		// _fgetc_nolock ���� ��������� ������������
		operation_type = _fgetc_nolock(INPUT_FD);

		if		(operation_type == 'O')  process_order();
		else if (operation_type == 'C')  process_cancel();
		else if (operation_type == '\n') continue;
		else if (operation_type == EOF)  break;
		else {
			printf("Invalid operation type: '%c'. Must be 'O' (order) or 'C' (cancel).", (char)operation_type);
			exit(PARSE_ERROR);
		}
		#ifdef DEBUG
		verify_trees();
		#endif // DEBUG
	}
}

/*
* ��������� ����� �� ����������. 
* ���� ���������� ���, �� ������� STDIN � STDOUT
*/
void open_files(const int argc, char* argv[]) {
	for (int i = 1; i < argc; i++) {
		// get input filename
		if (strcmp(argv[i], "-i") == 0) {
			if ((INPUT_FD = fopen(argv[++i], "r")) == NULL) {
				printf("Input file open error. File: %s", argv[i]);
				exit(FILE_ERROR);
			}
		}
		// get output filename
		else if (strcmp(argv[i], "-o") == 0) {
			if ((OUTPUT_FD = fopen(argv[++i], "wb")) == NULL) {
				printf("Output file open error. File: %s", argv[i]);
				exit(FILE_ERROR);
			}
		}
		else {
			printf("Invalid argument: %s", argv[i]);
			exit(INVALID_ARGUMENT);
		}
	}
	// �������� �� ���������
	if (!INPUT_FD) INPUT_FD = stdin;
	if (!OUTPUT_FD) OUTPUT_FD = stdout;
}

void close_files() {
	if ((INPUT_FD != stdin) && (fclose(INPUT_FD) == EOF)) {
		printf("Input file close error.");
		exit(FILE_ERROR);
	}
	if ((OUTPUT_FD != stdout) && (fclose(OUTPUT_FD) == EOF)) {
		printf("Output file close error.");
		exit(FILE_ERROR);
	}
}


/* -------------------------------------------------------------------------- */
/*									  MAIN									  */
/* -------------------------------------------------------------------------- */


int main(int argc, char* argv[]) {
	open_files(argc, argv);
	parse_lines();
	close_files();
	printf("Total time: %i", clock());
	exit(EXIT_SUCCESS);
}

