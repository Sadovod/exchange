/*
* PointerList - ��� ���������� ������, ������ ������� ��������, �������� ������ ����������
* �� ������ � ������������ ���������� id. �� ��������� ��� �������� ������� � ������ �� �� id.
* 
* � ������������ � � ���� ��� ������ "������" ��������������� ������� PointerList (�� ���� ������� ������).
* ���� � ������ ��� ��������� �� ������ ������� � ���� ������ �� ���������, �� �� ���������.
* ������ ������ ������ �����������.
*/


typedef struct PointerList {
	Order**				orders;					// ��������� �� ������
	uint32_t			non_null;				// ���-�� �� ������� ���������� �� ������
	uint32_t			min_id;
	uint32_t			max_id;						
	struct PointerList* next;					// ��������� ������ (NULL ��� ���������� ������)
	struct PointerList* prev;					// ���������� ������ (NULL ��� ������� ������)
} PointerList;


typedef struct {
	PointerList* first;							// ������ ������
	PointerList* last;
	uint32_t	 last_length;					// ����� ���������� ������
} PointerListRoot;


static PointerListRoot PListRoot = {
	.first = NULL,
	.last = NULL,
	.last_length = 100
};


void			plist_insert	(const Order* order);
Order*			plist_pop		(const uint32_t id);
PointerList*	_create_plist	(const uint32_t id);
void			_delete_plist	(PointerList* plist);