#include <stdint.h>
#include <stdlib.h>
#include "order_tree.h"
#include "pointer_list.h"


/*
* ������� ��������� ����� ������ ������ ��� "super_parent" 
* ���� ��������� ������ �������� ������ ��� "super_parent".
* ������ "super_parent" �������� �������, �� ���� ���,
* �� ������ ���� ���������� NULL � ������� ���� �������
* "super_parent" ��� ���������� ������.
* ����� ������� ���������� �������� ������.
* � ������� "super_parent" �������� � ������������ �����.
*/
void remove_order(Order* order, Order* super_parent) {
	if (super_parent) {
		if (order->worse) super_parent->best = order->worse->best;
		else if (order->parent) super_parent->best = order->parent;
	}
	else if (!order->better && !order->best) {
		super_parent = order;

		while (!super_parent->best) {
			super_parent = super_parent->parent;
		}
		if (order->worse) super_parent->best = order->worse->best;
		else super_parent->best = order->parent;
	}
	_remove_order(order);
}

void _remove_order(Order* order) {
	Order* filling_order;

	if (order->worse) {
		// ��� ���������� ��������� ������ �� ������� � �� "worse" ������
		// � ����� ������ ����� ��� ��� ���� "worse" ������
		filling_order = order->worse->best;
		// ���� ������ ������ ����� "worse" ������ ��� �� ��� ����
		if (filling_order != order->worse) {
			// ���� � ����������� ������ ���� ������ "worse"
			if (filling_order->worse) {
				filling_order->parent->better = filling_order->worse;
				filling_order->worse->parent = filling_order->parent;
				order->worse->best = filling_order->worse->best;
				filling_order->worse->best = NULL;
			}
			else {
				order->worse->best = filling_order->parent;
				filling_order->parent->better = NULL;
			}
			filling_order->worse = order->worse;
			order->worse->parent = filling_order;
		}
		// "better" ������ ��� ��������� ���������� "better" ������� ��� �����������
		if (order->better) {
			filling_order->better = order->better;
			order->better->parent = filling_order;
		}
	}
	// ������ "better" ��� NULL ���� "better" ���
	else filling_order = order->better;

	// ���� � ��������� ������ ���� ��������
	if (order->parent) {
		if (order->parent->better == order) {
			order->parent->better = filling_order;
		}
		else order->parent->worse = filling_order;
	}
	else order->root->first = filling_order;

	// ��������� best ������ � �������� ��� ����������� ������
	if (filling_order) {
		if (order->better) filling_order->best = order->best;
		else {
			if (order->parent && (order->parent->better == filling_order)) filling_order->best = NULL;
			else filling_order->best = filling_order;
		}
		filling_order->parent = order->parent;
	}
	free(order);
}

/*
* ��������� ������ � ������
* ���������������, ��� � ������ ��� ���� ���� �� ���� ������
*/
void tree_insert(Order* order) {
	OrdersRoot* root = order->root;
	Order* current_order = root->first;		// ������� ������ (�� ������������ ��� ������ �� ������)
	Order* super_parent = current_order;	// ������ ��������, �� �������� ����� ����� �� ������� ������ �� �������� � "worse"

	// ��������� ��� ����������� ������ ���� ����� ����������� ������� � �������
	// �������� ��� SELL ������ ����� ����� ��, ������� �������.
	int64_t x = (root->side == 'B') ? 1 : -1;
	int64_t y = -x;
	int64_t _price = x * (int64_t)order->price;

	while (1) {

		// ���� ���� ����������� ������ ����� ���� ������� ������
		if ((_price + (y * (int64_t)current_order->price)) > 0) {
			// ���� ���� ������ ����� ������� - ��� ���������� �������
			if (current_order->better) {
				current_order = current_order->better;
			}
			else {
				current_order->better = order;
				order->parent = current_order;
				super_parent->best = order;
				break;
			}
		}
		else {
			if (current_order->worse) {
				current_order = current_order->worse;
				super_parent = current_order;
			}
			else {
				current_order->worse = order;
				order->parent = current_order;
				order->best = order;
				break;
			}
		}
	}
}


// ������� ������ � ��������� � ������ ������
Order* save_order(OrdersRoot* root, const id,const uint32_t qty,const uint32_t price) {
	Order* order = calloc(1, sizeof(Order));
	if (!order) exit(EXIT_FAILURE);

	order->id = id;
	order->qty = qty;
	order->price = price;
	order->root = root;
	
	// ���� � ������ ��� ��� ������ - ��� ���������� ������
	if (!root->first) {
		root->first = order;
		order->best = order;
	} 
	else tree_insert(order);
	return order;
}